﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionar
{
    class Question
    {
        public int level { get; }
        public string questionsText { get; }
        public List<string> answers { get; }
        public Question(string questionsText, List<string> answers, int level)
        {
            this.questionsText = questionsText;
            this.answers = answers;
            this.level = level;
        }
    }
}
