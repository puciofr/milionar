﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionar
{
    class Player
    {
        public int score { get; }
        public string name { get; }
        public Player(string name, int score)
        {
            this.score = score;
            this.name = name;
        }
    }
}
