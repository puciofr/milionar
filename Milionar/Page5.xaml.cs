﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Milionar
{
    /// <summary>
    /// Interakční logika pro Page5.xaml
    /// </summary>
    public partial class Page5 : Page
    {
        public Page5()
        {
            InitializeComponent();

            Filescore filescore = new Filescore();

            foreach (var playerscore in filescore.FileScoreRead())
            {
                scoreBoard.Children.Add(new Button() {
                    Content = playerscore.name + " ( Score: " + playerscore.score + ")"
                });
            }
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.frame.Navigate(new Page2());
        }
    }
}
