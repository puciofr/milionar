﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Milionar
{
    /// <summary>
    /// Interakční logika pro Page2.xaml
    /// </summary>
    public partial class Page2 : Page
    {

        SoundPlayer player = new SoundPlayer("Music/starting_music.wav");
       
        public Page2()
        {
            InitializeComponent();

            player.Load();
            player.Play();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Opravdu si přejete ukončit hru?", "Potvrzení", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            player.Stop();
            MainWindow.frame.Navigate(new Page1());
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            player.Stop();
            MainWindow.frame.Navigate(new Page5());
        }

    }
}
