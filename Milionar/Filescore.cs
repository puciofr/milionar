﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milionar
{
    class Filescore
    {
        public void FileScoreWrite(string name,int levelQuestion)
        {

            List<Player> score = FileScoreRead();
            score.Add(new Player(name, levelQuestion));

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            string json = JsonConvert.SerializeObject(score, settings);
            File.WriteAllText(@"Players_Score.json", json);
        }

        public List<Player> FileScoreRead()
        {
            if (File.Exists(@"Players_Score.json"))
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                };

                string jsonFromFile = File.ReadAllText("Players_Score.json");

                List<Player> ScoreFromFile = JsonConvert.DeserializeObject<List<Player>>(jsonFromFile, settings);

                return ScoreFromFile;
            }
            return new List<Player>();
        }
    }
}
