﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Milionar
{
    /// <summary>
    /// Interakční logika pro Page4.xaml
    /// </summary>
    public partial class Page4 : Page
    {
        SoundPlayer player = new SoundPlayer("Music/wingame_music.wav");
        public Page4()
        {
            InitializeComponent();

            player.Load();
            player.Play();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Filescore filescore = new Filescore();
            filescore.FileScoreWrite(nameInput.Text, App.LevelQuestion);
            App.LevelQuestion = 1;

            MainWindow.frame.Navigate(new Page2());
        }

    }
}
