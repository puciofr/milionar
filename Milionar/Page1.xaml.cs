﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Milionar
{
    /// <summary>
    /// Interakční logika pro Page1.xaml
    /// </summary>
    public partial class Page1 : Page
    {
        private MediaPlayer player = new MediaPlayer();

        private int indexCorrectAnswer;
        private List<Question> questionList;
        private List<Label> currentRewardLevel;
        public Page1()
        {
            InitializeComponent();
            //fileWrite();
            questionList = fileRead();
            nextQuestion();

            currentRewardLevel = new List<Label>()
            {
                level1Label, level2Label, level3Label, level4Label, level5Label, level6Label, level7Label, level8Label, level9Label, level10Label, level11Label, level12Label, level13Label, level14Label, level15Label
            };

            PlaybackMusic();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show("Opravdu si přejete jít do menu? Váš postup ve hře nebude uložen!", "Potvrzení", System.Windows.MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                player.Stop();
                MainWindow.frame.Navigate(new Page2());
            }
            
        }

        private Question randomQuestion()
        {
            Random random = new Random();
            List<Question> levelQuestions = new List<Question>();

            foreach (Question question in questionList)
            {
                if (question.level == App.LevelQuestion)
                {
                    levelQuestions.Add(question);
                }
            }
            if (levelQuestions.Count > 0)
            {
                return levelQuestions[random.Next(levelQuestions.Count)];
            }
            else
            {
                return null;
            }
        }

        private void nextQuestion()
        {
            Question question = randomQuestion();
            if (question != null)
            {
                if(App.LevelQuestion > 1)
                {
                    currentRewardLevel[App.LevelQuestion - 1].Foreground = Brushes.Yellow;
                    currentRewardLevel[App.LevelQuestion - 2].Foreground = Brushes.Black;
                }

                List<string> randomizedAnswers = RandomAnswers(question.answers);

                questionBlock.Text = question.questionsText;
                firstAnswer.Content   = randomizedAnswers[0];
                secondAnswer.Content  = randomizedAnswers[1];
                thirdAnswer.Content   = randomizedAnswers[2];
                fourthAnswer.Content  = randomizedAnswers[3];
            } else
            {
                // upozorní hráče na nenalezené otázky
            }
        }

        public List<string> RandomAnswers(List<string> sortAnswers)
        {
            List<string> randomAnswers = new List<string>();

            bool firstTime = true;
            int randomIndex = 0;
            while (sortAnswers.Count > 0)
            {
                Random random = new Random();
                randomIndex = random.Next(0, sortAnswers.Count);
                randomAnswers.Add(sortAnswers[randomIndex]);
                sortAnswers.RemoveAt(randomIndex);

                if (firstTime && randomIndex == 0)
                {
                    indexCorrectAnswer = randomAnswers.Count - 1;
                    firstTime = false;
                }
            }

            return randomAnswers;
        }

        private void fileWrite()
        {
            List<Question> questions = new List<Question>();
            questions.Add(new Question("Pro jeden z druhů polibků se vžilo označení:", new List<string>() { "Francouzský", "Ruský", "Italský", "Německý" }, 1));
            questions.Add(new Question("Co je to ukulele?", new List<string>() { "Nástroj strunný", "Vzácné luční kvítí", "Bulharské hory", "Jezero v Norsku" }, 2));
            questions.Add(new Question("Ratatouille je podle původní receptury pokrm:", new List<string>() { "Ze zeleniny", "Z těstovin", "Z hub", "Z rýže" }, 3));
            questions.Add(new Question("Zpěvačka Ewa Farna v jedné ze svých písní zpívá, že má boky jako:", new List<string>() { "Skříň", "Vagón", "Almaru", "Gauč" }, 4));
            questions.Add(new Question("Kolik nul má dohromady numerický zápis jedné miliardy?", new List<string>() { "9", "6", "7", "8" }, 5));
            questions.Add(new Question("Rakouský hudebník Parov Stelar patří mezi nejznámější představitele:", new List<string>() { "Electro swingu", "Lyrického rocku", "Lidové hudby", "Heavy metalu" }, 6));
            questions.Add(new Question("Abych si zachránili život při nouzovém seskoku z hořícího letadla, rozhodně potřebujeme:", new List<string>() { "Padák", "Závěť", "Růženec", "Tranzistorák" }, 7));
            questions.Add(new Question("Rostlina hluchavka je mimo jiné charakteristická i tím, že:", new List<string>() { "Léčí neduhy", "Pojídá hmyz", "Dorůstá až 9 metrů", "Má modré květy" }, 8));
            questions.Add(new Question("Na horu Praděd, vysokou 1491m, byste mohli vystoupat:", new List<string>() { "V Jeseníkách", "V Krkonoších", "V Beskydech", "V Brdech" }, 9));
            questions.Add(new Question("Budeme-li vlastnit toku, potom bude mít:", new List<string>() { "Klobouk", "Ptáka", "Ovoce", "Dýmku" }, 10));
            questions.Add(new Question("Z čeho se vyrábí brandy?", new List<string>() { "Z hroznů", "Z ječmene", "Ze žita", "Z jalovce" }, 11));
            questions.Add(new Question("Měnou jednotkou Chorvatské republiky je:", new List<string>() { "Kuna", "Šakal", "Dinár", "Vlk" }, 12));
            questions.Add(new Question("K seriálu Včelka Mája složil hudbu český skladatel:", new List<string>() { "Karel Svoboda", "Ivan Hlas", "Zdeněk Merta", "Jaroslav Uhlíř" }, 13));
            questions.Add(new Question("Který nápis naleznete na láhvi vína v běžné vinotéce?", new List<string>() { "Včasný sběr", "Ledové víno", "S přívlastkem", "Výběr z hroznů" }, 14));
            questions.Add(new Question("Který stát není v USA, ale v Austrálii?", new List<string>() { "New South Wales", "New Mexico", "New Jersey", "New Hampshire" }, 15));
            questions.Add(new Question("V roce 1933 vzniklý koncentrační tábor Dachau se nacházel:", new List<string>() { "V Německu", "V Polsku", "V Rakousku", "V Dánsku" }, 1));

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            string json = JsonConvert.SerializeObject(questions, settings);
            File.WriteAllText(@"Milionar.json", json);
        }

        private List<Question> fileRead()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };

            string jsonFromFile = File.ReadAllText("Milionar.json");

            List<Question> questionsFromFile = JsonConvert.DeserializeObject<List<Question>>(jsonFromFile, settings);

            return questionsFromFile;
        }


        private void rightOrWrong(int answerNumber)
        {
            if (answerNumber == indexCorrectAnswer)
            {
                if (App.LevelQuestion != 15)
                {
                    App.LevelQuestion++;         
                    nextQuestion();

                    if (App.LevelQuestion == 6 || App.LevelQuestion == 11 || App.LevelQuestion == 15)
                    {
                        PlaybackMusic();
                    }

                } else
                {
                    player.Stop();
                    MainWindow.frame.Navigate(new Page4());
                }
            } else
            {
                player.Stop();
                MainWindow.frame.Navigate(new Page3());

                if (App.LevelQuestion > 10)
                {
                    App.LevelQuestion = 10;
                }
                else if (App.LevelQuestion > 5)
                {
                    App.LevelQuestion = 5;
                } else
                {
                    App.LevelQuestion = 1;
                }
            }
        }

        private void firstAnswer_Click(object sender, RoutedEventArgs e)
        {
            rightOrWrong(0);
        }

        private void secondAnswer_Click(object sender, RoutedEventArgs e)
        {
            rightOrWrong(1);
        }

        private void thirdAnswer_Click(object sender, RoutedEventArgs e)
        {
            rightOrWrong(2);
        }

        private void fourthAnswer_Click(object sender, RoutedEventArgs e)
        {
            rightOrWrong(3);
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            player.Play();
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            player.Pause();
        }

        private void Media_Ended(object sender, EventArgs e)
        {
            player.Position = TimeSpan.Zero;
            player.Play();
        }

        public void PlaybackMusic()
        {
            if (player != null)
            {
                if (App.LevelQuestion <= 5)
                {
                    player.Open(new Uri("Music/midgame_music.wav", UriKind.Relative));
                    player.Play();
                }
                else if (App.LevelQuestion >= 6 && App.LevelQuestion < 11)
                {
                    player.Open(new Uri("Music/lategame_music.wav", UriKind.Relative));
                    player.Play();
                }
                else if (App.LevelQuestion >= 11 && App.LevelQuestion < 15)
                {
                    player.Open(new Uri("Music/endgame_music.wav", UriKind.Relative));
                    player.Play();
                }
                else if (App.LevelQuestion == 15)
                {
                    player.Open(new Uri("Music/finalgame_music.wav", UriKind.Relative));
                    player.Play();
                }
                player.MediaEnded += new EventHandler(Media_Ended);

                return;
            }
        }


    }
}
